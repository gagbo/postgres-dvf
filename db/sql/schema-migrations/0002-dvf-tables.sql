CREATE SCHEMA dvf;

CREATE TABLE dvf.adresse(
       -- Adresse
       id serial PRIMARY KEY,
       numero_voie int,
       indice_repetition varchar(6),
       type_voie varchar(4),
       code_voie varchar(10),
       nom_voie varchar(40),
       code_postal int,
       commune varchar(40),
       code_dept varchar(4),
       code_commune int
);

CREATE TABLE dvf.bien(
       -- Bien at a given address
       id serial PRIMARY KEY,
       adresse_id int REFERENCES dvf.adresse(id),
       prefixe_section varchar(10),
       id_section varchar(10),
       numero_plan int
);

CREATE TABLE dvf.subtransaction(
       -- Transaction from the dvf file
       -- Will list each subtransaction for a single building
       id serial PRIMARY KEY,
       adresse_id int REFERENCES dvf.adresse(id),
       bien_id int REFERENCES dvf.bien(id),
       disposition_number int,
       mutation_date date,
       mutation_kind varchar(40),
       valeur_fonciere float,
       numero_volume varchar(10),
       lot_1 varchar(40),
       surface_lot_1 float,
       lot_2 varchar(40),
       surface_lot_2 float,
       lot_3 varchar(40),
       surface_lot_3 float,
       lot_4 varchar(40),
       surface_lot_4 float,
       lot_5 varchar(40),
       surface_lot_5 float,
       nombre_lots int,
       code_local int,
       type_local varchar(40),
       surface_bati float,
       nombre_pieces int,
       code_nature_culture varchar(10),
       nature_culture_sp varchar(40),
       surface_terrain float
);
