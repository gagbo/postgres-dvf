CREATE INDEX adresse_dept ON dvf.adresse USING BTREE (code_dept);
CREATE INDEX adresse_ville ON dvf.adresse USING BTREE (code_postal, commune);

CREATE INDEX bien_adresse ON dvf.bien USING BTREE (adresse_id);

CREATE INDEX subtransaction_adresse ON dvf.subtransaction USING BTREE (adresse_id, mutation_date);
CREATE INDEX subtransaction_surface_bati ON dvf.subtransaction USING BTREE (surface_bati);
CREATE INDEX subtransaction_surface_terrain ON dvf.subtransaction USING BTREE (surface_terrain);
CREATE INDEX subtransaction_prix ON dvf.subtransaction USING BTREE (valeur_fonciere);
