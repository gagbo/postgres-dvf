ALTER TABLE dvf.adresse
      ADD CONSTRAINT adresse_identique UNIQUE (code_postal,
                                                commune,
                                                code_dept,
                                                code_commune,
                                                nom_voie,
                                                code_voie,
                                                type_voie,
                                                indice_repetition,
                                                numero_voie);
ALTER TABLE dvf.bien
      ADD CONSTRAINT bien_identique UNIQUE (adresse_id,
                                            prefixe_section,
                                            id_section,
                                            numero_plan);
