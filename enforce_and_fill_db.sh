#!/usr/bin/env bash

BASE_FOLDER=$(dirname "$0")

"${BASE_FOLDER}/scripts/down.sh" && "${BASE_FOLDER}/scripts/up.sh"
for datafile in "$@"; do
    "${BASE_FOLDER}/scripts/fill_db.py" "${datafile}" || (printf "Error in python script !\n" && exit 1)
done

printf "DB is ready !\n"
exit 0
