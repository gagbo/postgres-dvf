#!/usr/bin/env bash

SCRIPTS_FOLDER=$(dirname "$0")

pushd "${SCRIPTS_FOLDER}"/../db || exit
docker-compose -p mowgli down --rmi local --remove-orphans
popd || exit
