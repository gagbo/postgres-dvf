#!/usr/bin/env python3

import psycopg2
import gzip
from enum import IntEnum, auto
import datetime as dt
import argparse
import os
import logging


def int_or_null(value: str):
    "Transform a string to either an int or None if string is empty"
    if value:
        return int(value)
    return None


def float_or_null(value: str):
    "Transform a string to either a float or None if string is empty"
    if value:
        return float(value)
    return None


def format_date(value: str):
    "Format a string to an ISO date. Needs to take the french format as input"
    date = dt.datetime.strptime(value, "%d/%m/%Y")
    return date.strftime("%Y-%m-%d")


def string_or_null(value: str):
    "Return the string wrapped in double-quotes, or \"NULL\" if the string is empty"
    if value:
        return f'{value}'
    return None


# Helper names for DVF files fields
class DvfFileFields(IntEnum):
    # _R suffix means "restricted" (should be empty)
    CODE_SERVICE_CH_R = 0
    REF_DOCUMENT_R = auto()
    ARTICLE_CGI_1_R = auto()
    ARTICLE_CGI_2_R = auto()
    ARTICLE_CGI_3_R = auto()
    ARTICLE_CGI_4_R = auto()
    ARTICLE_CGI_5_R = auto()
    NO_DISPOSITION = auto()
    DATE_MUTATION = auto()
    NATURE_MUTATION = auto()
    VALEUR_FONCIERE = auto()
    NO_VOIE = auto()
    INDICE_REP = auto()
    TYPE_VOIE = auto()
    CODE_VOIE = auto()
    VOIE = auto()
    CODE_POSTAL = auto()
    COMMUNE = auto()
    CODE_DEPT = auto()
    CODE_COMMUNE = auto()
    PREFIXE_SECTION = auto()
    SECTION = auto()
    NO_PLAN = auto()
    NO_VOLUME = auto()
    LOT_1 = auto()
    SURFACE_CARREZ_LOT_1 = auto()
    LOT_2 = auto()
    SURFACE_CARREZ_LOT_2 = auto()
    LOT_3 = auto()
    SURFACE_CARREZ_LOT_3 = auto()
    LOT_4 = auto()
    SURFACE_CARREZ_LOT_4 = auto()
    LOT_5 = auto()
    SURFACE_CARREZ_LOT_5 = auto()
    NOMBRE_LOTS = auto()
    CODE_TYPE_LOCAL = auto()
    TYPE_LOCAL = auto()
    IDENTIFIANT_LOCAL_R = auto()
    SURFACE_REELLE_BATI = auto()
    NOMBRE_PIECES_PRINCIPALES = auto()
    CODE_NATURE_CULTURE = auto()
    NATURE_CULTURE_SPECIALE = auto()
    SURFACE_TERRAIN = auto()
    NUMBER_OF_FIELDS = auto() # Using 0-based index to get the len


SQL_FUNCTIONS_FP = os.path.join(os.path.dirname(os.path.abspath(__file__)), "insert_functions.sql")


def insert_line(line: str):
    "Insert a line from a data.gouv.fr file into the base using a pgpsql call."
    dvf_split_line = line.replace(",", ".").strip().rstrip().split("|")
    assert len(dvf_split_line) == DvfFileFields.NUMBER_OF_FIELDS
    cur.execute(
        f"""
    SELECT * FROM insert_line(
    %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,
    %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,
    %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,
    %s, %s);
    """,
        (
            int_or_null(dvf_split_line[DvfFileFields.NO_VOIE]),
            string_or_null(dvf_split_line[DvfFileFields.INDICE_REP]),
            string_or_null(dvf_split_line[DvfFileFields.TYPE_VOIE]),
            string_or_null(dvf_split_line[DvfFileFields.CODE_VOIE]),
            string_or_null(dvf_split_line[DvfFileFields.VOIE]),
            int_or_null(dvf_split_line[DvfFileFields.CODE_POSTAL]),
            string_or_null(dvf_split_line[DvfFileFields.COMMUNE]),
            string_or_null(dvf_split_line[DvfFileFields.CODE_DEPT]),
            int_or_null(dvf_split_line[DvfFileFields.CODE_COMMUNE]),
            string_or_null(dvf_split_line[DvfFileFields.PREFIXE_SECTION]),
            string_or_null(dvf_split_line[DvfFileFields.SECTION]),
            int_or_null(dvf_split_line[DvfFileFields.NO_PLAN]),
            int_or_null(dvf_split_line[DvfFileFields.NO_DISPOSITION]),
            format_date(dvf_split_line[DvfFileFields.DATE_MUTATION]),
            string_or_null(dvf_split_line[DvfFileFields.NATURE_MUTATION]),
            float_or_null(dvf_split_line[DvfFileFields.VALEUR_FONCIERE]),
            string_or_null(dvf_split_line[DvfFileFields.NO_VOLUME]),
            string_or_null(dvf_split_line[DvfFileFields.LOT_1]),
            float_or_null(dvf_split_line[DvfFileFields.SURFACE_CARREZ_LOT_1]),
            string_or_null(dvf_split_line[DvfFileFields.LOT_2]),
            float_or_null(dvf_split_line[DvfFileFields.SURFACE_CARREZ_LOT_2]),
            string_or_null(dvf_split_line[DvfFileFields.LOT_3]),
            float_or_null(dvf_split_line[DvfFileFields.SURFACE_CARREZ_LOT_3]),
            string_or_null(dvf_split_line[DvfFileFields.LOT_4]),
            float_or_null(dvf_split_line[DvfFileFields.SURFACE_CARREZ_LOT_4]),
            string_or_null(dvf_split_line[DvfFileFields.LOT_5]),
            float_or_null(dvf_split_line[DvfFileFields.SURFACE_CARREZ_LOT_5]),
            int_or_null(dvf_split_line[DvfFileFields.NOMBRE_LOTS]),
            int_or_null(dvf_split_line[DvfFileFields.CODE_TYPE_LOCAL]),
            string_or_null(dvf_split_line[DvfFileFields.TYPE_LOCAL]),
            float_or_null(dvf_split_line[DvfFileFields.SURFACE_REELLE_BATI]),
            int_or_null(dvf_split_line[DvfFileFields.NOMBRE_PIECES_PRINCIPALES]),
            string_or_null(dvf_split_line[DvfFileFields.CODE_NATURE_CULTURE]),
            string_or_null(dvf_split_line[DvfFileFields.NATURE_CULTURE_SPECIALE]),
            float_or_null(dvf_split_line[DvfFileFields.SURFACE_TERRAIN]),
        ),
    )


def parse_arguments():
    "Parse command line arguments and return a parsed Namespace"
    parser = argparse.ArgumentParser(description="Fill a postgres database with data from https://cadastre.data.gouv.fr/dvf#download")
    parser.add_argument("gzipped_dvf_filepath", type=str, help="Relative path to gzipped file from data.gouv.fr", default="data/sample.txt.gz")
    parser.add_argument("--pghost", type=str, help="host of the postgres server", default="localhost")
    parser.add_argument("--user", type=str, help="Username to connect to database", default="mowgli")
    parser.add_argument("--password", type=str, help="Password of user connection", default="example")
    parser.add_argument("--dbname", type=str, help="Name of the database", default="immo")
    parser.add_argument("--log", dest="loglevel", type=str, help="Logging level. Use valid values from python logging module", default="INFO")
    return parser.parse_args()


def setup_logging(logging_level: str):
    "Set the logging up according the level given in logging_level"
    numeric_level = getattr(logging, config.loglevel.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError('Invalid log level: %s' % config.loglevel)
    logging.basicConfig(level=numeric_level)


if __name__ == "__main__":
    config = parse_arguments()
    setup_logging(config.loglevel.upper())
    logger = logging.getLogger(__name__)
    logger.debug("%s", config)

    connection_args = {
        "dbname": config.dbname,
        "password": config.password,
        "user": config.user,
        "host": config.pghost,
    }
    datafile = os.path.join(os.getcwd(), config.gzipped_dvf_filepath)

    conn = psycopg2.connect(**connection_args)
    cur = conn.cursor()
    with open(SQL_FUNCTIONS_FP, "r") as sql_functions_file:
        cur.execute(sql_functions_file.read())

    line_counter = 0
    with gzip.open(datafile, "r") as f:
        for line in f:
            line_counter += 1
            insert_line(line.decode("utf-8"))
            if line_counter % 100000 == 0:
                logger.info("Line %s processed !", line_counter)

    print(f"File {datafile} has been processed")
    conn.commit()
