#!/usr/bin/env bash
# Helper script to create a random sample file from a yearly record

SCRIPTS_FOLDER=$(dirname "$0")

main() {
    zcat "$1" | awk "BEGIN {srand()} !/^\$/ { if (rand() <= $2) print \$0}" > "$3" && gzip "$3"
}

main "${1:-${SCRIPTS_FOLDER}/../data/valeursfoncieres-2018.txt.gz}" "${2:-0.02}" "${3:-${SCRIPTS_FOLDER}/../data/sample.txt}"
