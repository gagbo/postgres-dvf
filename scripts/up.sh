#!/usr/bin/env bash

# Usage : wait_for_postgres host port timeout
wait_for_postgres() {
    host="$1"
    port="$2"
    timeout="$3"
    retry_timer=2

    if ! command -v pg_isready >/dev/null 2>&1; then
        >&2 printf "pg_isready is unavailable. Waiting for %d seconds plainly.\n" "$timeout"
        sleep "$timeout"
        return
    fi

    >&2 printf "Using pg_isready to check for postgres availability - sleeping for %d seconds between each try (.)\n" "$retry_timer"
    until pg_isready -h "$host" -p "$port" >/dev/null 2>/dev/null; do
        >&2 printf "."
        sleep $retry_timer
    done

    >&2 printf "\nPostgres (%s:%d) is up !\n" "$host" "$port"
}

SCRIPTS_FOLDER=$(dirname "$0")

pushd "${SCRIPTS_FOLDER}"/../db || exit
docker build -t pg-mowgli .

docker-compose -p mowgli up -d --remove-orphans
wait_for_postgres "localhost" "5432" 30
popd || exit
